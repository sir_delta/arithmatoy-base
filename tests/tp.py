# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return f"{'S' * n}0"


def S(n: str) -> str:
    return f"S{n}"


def addition(a: str, b: str) -> str:
    if a == '0':
        return b
    return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == '0':
        return '0'
    return addition(multiplication(a[1:], b), b)


def facto_ite(n: int) -> int:
    r = 1
    for i in range(1, n + 1):
        r = r * i
    return r


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n < 2:
        return n
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    a = 0
    b = 1
    for i in range(0, n):
        c = a + b
        a = b
        b = c
    return a


def golden_phi(n: int) -> int:
    return fibo_ite(n + 1) / fibo_ite(n)


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    return a * pow(a, n - 1)
