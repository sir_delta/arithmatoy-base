#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

size_t MAX_NUMBER_DIGITS = 512;
int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  char *num        = NULL;  // The output buffer.
  unsigned int val = 0;     // A temporary result.
  unsigned int li  = 0;     // The position of a digit in the LHS number.
  unsigned int ri  = 0;     // The position of a digit in the RHS number.
  unsigned int ni  = 0;     // The position of a digit in the output number.

  // Initialize elements.

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  if (!num) {
    debug_abort("malloc() failed");
  }
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  // Do the addition.
  // The operation is performed from left to right.

  while (li > 0 || ri > 0) {
    unsigned int carry = val;

    if (li > 0) {
      li--;
      val += get_digit_value(lhs[li]);
    }
    if (ri > 0) {
      ri--;
      val += get_digit_value(rhs[ri]);
    }

    num[ni] = to_digit(val % base);
    val    /= base;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n",
        ( li > 0 ? lhs[li] : to_digit(0) ),
        ( ri > 0 ? rhs[ri] : to_digit(0) ),
        carry);
      fprintf(stderr, "add: result: digit %c carry %u\n", num[ni], val);
    }

    ni++;
  }

  if (val > 0) {
    if (VERBOSE) {
      fprintf(stderr, "add: final carry %u\n", val);
    }
    num[ni] = to_digit(val);
  }

  // Invert the output buffer and skip the leading zeros.

  return drop_leading_zeros(reverse(num));
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  char *num        = NULL;  // The output buffer.
   int val         = 0;     // A temporary result.
  unsigned int li  = 0;     // The position of a digit in the LHS number.
  unsigned int ri  = 0;     // The position of a digit in the RHS number.
  unsigned int ni  = 0;     // The position of a digit in the output number.

  // Initialize elements.

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  if (!num) {
    debug_abort("malloc() failed");
  }
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  // Do the substraction.
  // The operation is performed from left to right.

  while (li > 0) {
    int carry = val;

    if (ri > 0) {
      ri--;
      val += get_digit_value(rhs[ri]);
    }
    val = get_digit_value(lhs[li - 1]) - val;

    if (val < 0) {
      if (li == ri) {
        break;
      }
      num[ni] = to_digit(val + base);
      val = 1;
    } else {
      num[ni] = to_digit(val);
      val = 0;
    }

    li--;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n",
        ( li > 0 ? lhs[li] : to_digit(0) ),
        ( ri > 0 ? rhs[ri] : to_digit(0) ),
        carry);
      fprintf(stderr, "add: result: digit %c carry %u\n", num[ni], val);
    }

    ni++;
  }

  // If the LHS number has more digit than the RHS number...
  if (val > 0) {
    if (VERBOSE) {
      fprintf(stderr, "sub: cannot process: %s < %s\n", lhs, rhs);
    }
    arithmatoy_free(num);
    return NULL;
  }

  // Invert the output buffer and skip the leading zeros.

  return drop_leading_zeros(reverse(num));
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  char *num        = NULL;  // The output buffer.
  unsigned int val = 0;     // A temporary result.
  unsigned int li  = 0;     // The position of a digit in the LHS number.
  unsigned int ri  = 0;     // The position of a digit in the RHS number.
  unsigned int ni  = 0;     // The position of a digit in the output number.

  // Initialize elements.

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  if (!num) {
    debug_abort("malloc() failed");
  }
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  // Do the multiplication.
  // The operation is performed from left to right.

  while (ri > 0) {
    int carry = val;
    int l     = li;
    int n     = ni;

    while (l > 0) {
      val += get_digit_value(lhs[l - 1]) * get_digit_value(rhs[ri - 1]);

      if (num[n] != '\0') {
        val += get_digit_value(num[n]);
      }

      num[n] = to_digit(val % base);

      val /= base;
      l--;
      n++;
    }

    if (val > 0)
      num[n] = to_digit(val);

    ri--;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n",
        ( li > 0 ? lhs[li] : to_digit(0) ),
        ( ri > 0 ? rhs[ri] : to_digit(0) ),
        carry);
      fprintf(stderr, "add: result: digit %c carry %u\n", num[ni], val);
    }

    val = 0;
    ni++;
  }

  // Invert the output buffer and skip the leading zeros.

  return drop_leading_zeros(reverse(num));
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
